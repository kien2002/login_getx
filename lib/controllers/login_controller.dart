import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  final GlobalKey<FormState> loginFormKey = GlobalKey<FormState>();
  late TextEditingController email_controller, pass_controller;
  var email = '';
  var pass = "";

  @override
  void onInit() {
    super.onInit();
    email_controller = TextEditingController();
    pass_controller = TextEditingController();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
    email_controller.dispose();
    pass_controller.dispose();
  }

  String? validateEmail(String value) {
    if (!GetUtils.isEmail(value)) {
      return "Please enter email";
    }
    return null;
  }

  String? validatePass(String? value) {
    if (value == null) {
      return "Please enter password";
    } else if (value.length < 6) {
      return "Password must be more than 6 characters";
    }
    return null;
  }

  Future<bool> checkLogin() async {
    final isValid = loginFormKey.currentState!.validate();
    if (isValid) {
      return true;
    }
    loginFormKey.currentState!.save();
    return false;
  }
}
