import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:login_with_getx/controllers/login_controller.dart';
import 'package:login_with_getx/screen/Home.dart';

class LoginView extends GetView<LoginController> {
  @override
  final controller = Get.put(LoginController());
  LoginView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
        margin: const EdgeInsets.only(top: 60, left: 16, right: 16),
        width: context.width,
        height: context.height,
        child: SingleChildScrollView(
            child: Form(
          key: controller.loginFormKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(children: [
            const Text(
              "Welcome to Get X",
              style: TextStyle(color: Colors.black38, fontSize: 20),
            ),
            const SizedBox(height: 20),
            TextFormField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10)),
                  labelText: "Email",
                  prefixIcon: const Icon(Icons.email_outlined)),
              keyboardType: TextInputType.emailAddress,
              controller: controller.email_controller,
              onSaved: (value) {
                controller.email = value!;
              },
              validator: (value) {
                return controller.validateEmail(value!);
              },
            ),
            const SizedBox(height: 20),
            TextFormField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10)),
                  labelText: "Password",
                  prefixIcon: const Icon(Icons.lock)),
              controller: controller.pass_controller,
              onSaved: (value) {
                controller.pass = value!;
              },
              validator: (value) {
                return controller.validatePass(value!);
              },
            ),
            const SizedBox(height: 20),
            ConstrainedBox(
              constraints: BoxConstraints.tightFor(width: context.width),
              child: ElevatedButton(
                style: ButtonStyle(
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
                    backgroundColor:
                        MaterialStateProperty.all(Colors.pinkAccent),
                    padding:
                        MaterialStateProperty.all(const EdgeInsets.all(14))),
                child: const Text(
                  "Login",
                  style: TextStyle(fontSize: 18, color: Colors.white),
                ),
                onPressed: () async {
                  bool loggedIn = await controller.checkLogin();
                  if (loggedIn) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => const Home()));
                  }
                },
              ),
            )
          ]),
        )),
      )),
    );
  }
}
